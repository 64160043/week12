package com.jidapa.week12;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class TestHashMap {
    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap();
        map.put("A1", "A1234517");
        map.put("B1", "A1234517");
        map.put("C1", "A1234517");
        map.put("D1", "A1234517");
        Set<String> keys = map.keySet();
        Iterator<String> iterator = keys.iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            System.out.println(key + "=" + map.get(key));
        }
        System.out.println(map.get("A1"));
        System.out.println(map.isEmpty());

    }

}
